# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

User.find_or_create_by_login 'john_d'
User.find_or_create_by_login 'anna_k'
User.find_or_create_by_login 'philippe_k'


if Rails.env.development?
  Message.destroy_all if ENV['PURGE_MESSAGES']

  puts "creating fake messages"
  Message.inject_5_fake_messages
  puts 'done'
end
