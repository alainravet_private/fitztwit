class AddAtomicReplyToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :in_reply_to_status_id,   :integer
    add_column :messages, :in_reply_to_screen_name, :string
  end

  def self.down
    remove_column :messages, :in_reply_to_screen_name
    remove_column :messages, :in_reply_to_status_id
  end
end
