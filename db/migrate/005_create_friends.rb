class CreateFriends < ActiveRecord::Migration
  def self.up
    create_table :friends do |t|
      t.string :screen_name
      t.string :name
      t.string :last_profile_image_url
      t.datetime :last_tweet_at

      t.timestamps
    end
  end

  def self.down
    drop_table :friends
  end
end
