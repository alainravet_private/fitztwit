class ChangeStatusIdToBigInt < ActiveRecord::Migration
  def self.up
    change_column :statuses, :status_id, :integer, :limit => 8
  end

  def self.down
  end
end
