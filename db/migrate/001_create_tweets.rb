class CreateTweets < ActiveRecord::Migration
  def self.up
    create_table :tweets do |t|
      t.integer   :tweet_id
      t.string    :screen_name
      t.string    :name
      t.string    :content
      t.datetime  :created
      t.string    :profile_image_url
    end
    add_index :tweets, [:screen_name, :created]
  end

  def self.down
    drop_table :tweets
  end
end