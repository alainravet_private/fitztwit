class RenameRetweetSource < ActiveRecord::Migration
  def self.up
    rename_column :messages, :origin_status_id, :retweet_source_id
    add_column    :messages, :retweet_source_status_id, :integer
    remove_column :messages, :retweet
  end

  def self.down
    add_column    :messages, :retweet, :boolean, :default => false, :null => false
    remove_column :messages, :retweet_source_status_id
    rename_column :messages, :retweet_source_id, :origin_status_id
  end
end

