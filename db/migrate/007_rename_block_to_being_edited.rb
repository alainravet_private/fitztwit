class RenameBlockToBeingEdited < ActiveRecord::Migration
  def self.up
    rename_column :messages, :blocked_at, :being_edited_at
    rename_column :messages, :blocked_by, :being_edited_by
  end

  def self.down
    rename_column :messages, :being_edited_at, :blocked_at
    rename_column :messages, :being_edited_by, :blocked_by
  end
end
