class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.integer   :created_by   , :null => false
      t.integer   :approved_by
      t.integer   :blocked_by

      t.text      :body         , :null => false
      t.string    :state        , :null => false, :limit => 20

      t.timestamps
      t.datetime  :blocked_at
      t.datetime  :approved_at
    end

    add_index :messages, :created_at
    add_index :messages, [:state, :created_at]
  end

  def self.down
    drop_table :messages
  end
end
