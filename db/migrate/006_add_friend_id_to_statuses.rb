class AddFriendIdToStatuses < ActiveRecord::Migration
  def self.up
    add_column :statuses, :friend_id, :integer

    add_index  :statuses, :friend_id
  end

  def self.down
    remove_column :statuses, :friend_id
  end
end
