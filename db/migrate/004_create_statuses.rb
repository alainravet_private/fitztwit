class CreateStatuses < ActiveRecord::Migration
  def self.up
    create_table :statuses do |t|
      t.integer :status_id
      t.string :screen_name
      t.string :name
      t.string :content, :limit => 200
      t.datetime :status_created_at
      t.string :profile_image_url
      t.string :source, :limit => 2

      t.timestamps
    end
    add_index :statuses, [:source, :status_id]
    add_index :statuses, [:status_id]
    add_index :statuses, [:screen_name, :status_created_at]
  end

  def self.down
    drop_table :statuses
  end
end
