class AddRetweetAndOriginStatusIdToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :retweet, :boolean, :default => false, :null => false
    add_column :messages, :origin_status_id, :integer
  end

  def self.down
    remove_column :messages, :origin_status_id
    remove_column :messages, :retweet
  end
end
