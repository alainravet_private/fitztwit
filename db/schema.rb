# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 11) do

  create_table "friends", :force => true do |t|
    t.string   "screen_name"
    t.string   "name"
    t.string   "last_profile_image_url"
    t.datetime "last_tweet_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", :force => true do |t|
    t.integer  "created_by",                             :null => false
    t.integer  "approved_by"
    t.integer  "being_edited_by"
    t.text     "body",                                   :null => false
    t.string   "state",                    :limit => 20, :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "being_edited_at"
    t.datetime "approved_at"
    t.integer  "retweet_source_id"
    t.integer  "retweet_source_status_id"
    t.integer  "in_reply_to_status_id"
    t.string   "in_reply_to_screen_name"
  end

  add_index "messages", ["created_at"], :name => "index_messages_on_created_at"
  add_index "messages", ["state", "created_at"], :name => "index_messages_on_state_and_created_at"

  create_table "statuses", :force => true do |t|
    t.integer  "status_id",         :limit => 8
    t.string   "screen_name"
    t.string   "name"
    t.string   "content",           :limit => 200
    t.datetime "status_created_at"
    t.string   "profile_image_url"
    t.string   "source",            :limit => 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "friend_id"
  end

  add_index "statuses", ["friend_id"], :name => "index_statuses_on_friend_id"
  add_index "statuses", ["screen_name", "status_created_at"], :name => "index_statuses_on_screen_name_and_status_created_at"
  add_index "statuses", ["source", "status_id"], :name => "index_statuses_on_source_and_status_id"
  add_index "statuses", ["status_id"], :name => "index_statuses_on_status_id"

  create_table "tweets", :force => true do |t|
    t.integer  "tweet_id"
    t.string   "screen_name"
    t.string   "name"
    t.string   "content"
    t.datetime "created"
    t.string   "profile_image_url"
  end

  add_index "tweets", ["screen_name", "created"], :name => "index_tweets_on_screen_name_and_created"

  create_table "users", :force => true do |t|
    t.string   "login"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
