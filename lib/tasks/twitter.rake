
include ActionView::Helpers::TextHelper #for pluralize


namespace :twitter do

  desc "sent the approved messages to Twitter"
  task :tweet_all_approved_messages => [:environment] do
    puts "Tweeting the approved messages...."
    nof_m = Message.approved.count
    before = Time.now

    begin
      nothing_to_tweet = 0 == nof_m
      @result = StatusUpdater.tweet_all_approved_messages unless nothing_to_tweet
      nof_tweeted = nof_m - Message.approved.count
      time_taken = (Time.now - before).to_s
      log_tweeting_result(nof_tweeted, time_taken, @result)
    rescue => e
      nof_tweeted = nof_m - Message.approved.count
      time_taken = (Time.now - before).to_s
      log_tweeting_error(e, time_taken, @result, nof_tweeted)
    end
  end



  desc 'call Twitter and import the new statuses from our friends_timeline'
  task :import_timeline_changes => [:environment] do

    puts "Importing our Timeline changes...."
    nof_f, nof_s = Friend.count, Status.count
    before = Time.now

    begin
      StatusFetcher.import_all

      new_f, new_s = Friend.count-nof_f, Status.count-nof_s
      time_taken = (Time.now - before).to_s
      log_import_result(new_f, new_s, time_taken)
    rescue => e
      time_taken = (Time.now - before).to_s
      log_import_error(e, time_taken)
      raise e
    ensure
    end

  end

private


  def log_tweeting_result(nof_tweeted, time_taken, result)
    puts "------------------------------------------------------------------------------------"
    msg = []

    if Message.approved.empty? && nof_tweeted == 0
      msg << "SUCCESS : (kind of : nothing to tweet) - it took #{time_taken}"

    elsif Message.approved.empty?
      msg << "SUCCESS : #{pluralize(nof_tweeted, "approved message")} tweeted.  - it took #{time_taken}"

    else
      msg << "FAILURE : could not tweet all the approved messages.  - it took #{time_taken}"
      msg << " - sent     : #{pluralize(nof_tweeted, "message")}."
      msg << " - not sent : #{pluralize(Message.approved.count, "message")}"
      msg << " - #{result}"
    end
    msg.each do |m| puts m end
    puts "------------------------------------------------------------------------------------"
  end


  def log_tweeting_error(e, time_taken, result, nof_tweeted)
    puts "------------------------------------------------------------------------------------"
    puts "*** ERROR while tweeting the approved messages : - it took #{time_taken}"
    puts "    success : " + pluralize(nof_tweeted, "message") + "were sent successfully"
    puts "    result  : " + result.to_s
    puts "    msg : " + e.to_s
    puts "------------------------------------------------------------------------------------"
  end


  def log_import_result(new_f, new_s, time_taken)
    puts "------------------------------------------------------------------------------------"
    puts "it took #{time_taken}"
    puts msg = (0 == new_f) ? 'no new friends'  : pluralize(new_f, 'new friend')
    puts msg = (0 == new_s) ? 'no new statuses' : pluralize(new_s, 'new statuses')
    puts "------------------------------------------------------------------------------------"
  end

  def log_import_error(e, time_taken)
    puts "------------------------------------------------------------------------------------"
    puts "*** ERROR while importing the Twitter Timeline Changes : it took #{time_taken}"
    puts "***    msg : " + e.to_s
    puts "------------------------------------------------------------------------------------"
  end
end
