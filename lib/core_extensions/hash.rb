class Hash
  def only(*whitelist)
    self.reject {|key, value| !whitelist.include?(key) }
  end

  # Author: Dave Nolan
  # symbolize hash keys, works with nested hashes (e.g. YAML)
  def rekey
    h = self.dup
    h.each_pair do |k, v|
      h.delete(k)
      k_sym = k.to_sym
      nk = k_sym || k
      nv = v.respond_to?(:rekey) ? v.rekey : v
      h[nk] = nv
    end
    h
  end
end