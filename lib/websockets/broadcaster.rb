#!/usr/bin/env ruby

require File.expand_path("../utils", __FILE__)

require "rubygems"
require "bundler/setup"
require 'em-websocket'


CONFIG = load_web_app_config

HOST, PORT = CONFIG['host'] || get_local_ip_address, CONFIG['port']
HEARTBEAT_DELAY = CONFIG['heartbeat_delay']

puts "\n\n\n\n"
puts "+--------------------------------------------------+"
puts "|               Websocket Broadcaster              |"
puts "+--------------------------------------------------+"
puts " wsb : starting.."
puts " wsb : host : #{HOST}"
puts " wsb : port : #{PORT}"
puts "+--------------------------------------------------+"

trap('INT') { $interrupted = true }

EventMachine.run {

    @channel = EM::Channel.new
    puts " wsb : channel opened #{@channel.to_s}"


    # The heartbeats-sender
    Thread.new do
      loop do
        if @interrupted
          puts "\n\nno longer sending heartbeats"
          puts "type Ctrl-C once more to stop the broadcaster"
          break
        end
        @channel.push 'heartbeat'
        sleep(HEARTBEAT_DELAY)
      end
    end


    @conn_count = 0
    EventMachine::WebSocket.start(:host => HOST, :port => PORT) do |ws|

        @conn_count += 1
puts " wsb : opening connection - @conn_count = #{@conn_count}"

        ws.onopen {
          sid = @channel.subscribe { |msg| ws.send msg }
          @channel.push "BROAD : #{sid} connected!"
          @channel.push "broadcaster:opening"

          ws.onmessage { |msg|
            @channel.push msg
          }


          ws.onclose {
            @channel.push "broadcaster:closing"
            @channel.unsubscribe(sid)
          }
        }

    end
}
