begin
  require 'json'
  require "web_socket"  #  gem install web-socket-ruby

rescue LoadError
  require 'rubygems'
  require 'json'
  require "web_socket"  #  gem install web-socket-ruby
end

#--------------------------------------------------------------------------------------------
class RemoteJquery
#--------------------------------------------------------------------------------------------
  attr_reader :host_url
  attr_reader :selector

  # Usage :
  #   rj = RemoteJquery.new("0.0.0.0", 8080)
  #
  def initialize(host, port)
    @host_url = "ws://#{host}:#{port}"
  end


  # Usage :
  #   remote_selector = RemoteJquery.new("0.0.0.0", 8080).selector(@message)
  #   remote_selector = RemoteJquery.new("0.0.0.0", 8080).selector("#flash")
  #   remote_selector.toggleClass('hilite')
  def selector(selector_or_ar)
    @selector = selector_or_ar.respond_to?(:dom_id) ?
        selector_or_ar.dom_id('#') :
        selector_or_ar
    self
  end

private
  # Usage :
  #   rem_send('toggleClass', :className => 'hilite')
  #
  def rem_send(fn, options={})
    msg     = JSON.generate(:fn => fn, :params => {:selector => @selector}.merge(options))
    begin
      #TODO : reuse the connection, don't create 1 per message to send
      client  = WebSocket.new(host_url)
      client.send(msg)
      client.close
    rescue Errno::ECONNREFUSED => e
      :connection_error
    end
  end

end


#--------------------------------------------------------------------------------------------
class RemoteJquery
#--------------------------------------------------------------------------------------------
  # !! the parameters names must match the ones found in websockets.js

  def alert(msg)              rem_send('alert', :msg => msg) end

  def toggle; rem_send('toggle') end
  def show  ; rem_send('show'  ) end
  def hide  ; rem_send('hide'  ) end
  def remove; rem_send('remove') end
  def hilite; rem_send('hilite') end


  # does not work !! jQuery problem ??
  def slideDown(duration='slow')
    rem_send('slideDown', :duration => duration)
  end

  def slideToggle(duration='slow')
    rem_send('slideToggle', :duration => duration)
  end

  def slideToggleAndRemove(duration='slow')
    rem_send('slideToggleAndRemove', :duration => duration)
  end

  def text(text)              rem_send('text'       , :htmlString => text     ) end
  def html(text)              rem_send('html'       , :htmlString => text     ) end

  def toggleClass(className)  rem_send('toggleClass', :className  => className) end
  def addClass(   className)  rem_send(   'addClass', :className  => className) end
  def removeClass(className)  rem_send('removeClass', :className  => className) end

  def disableLink  ;          rem_send('disableLink'                          ) end
  def disableButton;          rem_send('disableButton'                        ) end

  def effect(effect, options={}, speed=3000)
    rem_send('effect', :effect => effect, :options => options, :speed => speed                     )
  end

  #TODO : make this parametrizable - ex: call('update_approved')
  def call_update_approved
    rem_send('call_update_approved')
  end

  #TODO : make this parametrizable - ex: call('update_approved')
  def call_update_pendings
    rem_send('call_update_pendings')
  end
end

