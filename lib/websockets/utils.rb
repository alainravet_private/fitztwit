


def get_local_ip_address
  # same code as in config/initializers/app_config.rb
  require 'socket'
  UDPSocket.open {|s| s.connect('64.233.187.99', 1); s.addr.last }
end


def load_web_app_config
  require 'yaml'
  config_file =  File.expand_path("../../../config/app_config.yml", __FILE__)
  config = YAML.load_file(config_file)['default']['broadcaster']
  config['host'] ||= get_local_ip_address
  config
end

