require File.join(File.dirname(__FILE__), 'remote_jquery')

remoter   = RemoteJquery.new("0.0.0.0", 8080)

remoter.alert("this message was sent by demo.rb, relayed by broadcaster.rb, and received by your browser via websockets")
selector  = remoter.selector("#flash")
selector.html("00000")

sleep(2)

selector.removeClass('hilite')
selector.hilite

sleep(2)
selector.toggleClass("hilite")

sleep(2)
selector.addClass("hilite")

sleep(2)
selector.removeClass("hilite")

sleep(3)
remoter.selector("input[type=submit]").disableButton

sleep(3)
remoter.selector("a").disableLink

sleep(3)
remoter.selector("a").hide

sleep(3)
remoter.selector("a").toggle

sleep(3)
remoter.selector("a").show

sleep(3)
remoter.selector("a").remove
