var RemoteJquery = {
    alert :function(params){ alert(params.msg)  },

    text  :function(params){ $(params.selector).text(params.htmlString)  },
    html  :function(params){ $(params.selector).html(params.htmlString)  },
    toggle:function(params){ $(params.selector).toggle()  },
    hide  :function(params){ $(params.selector).hide()    },
    remove:function(params){ $(params.selector).remove()  },


    // slideDown DOES NOT WORK, but slideToggle does ???? TODO : INVESTIGATE AND FIX
    slideDown:function(params) {
      $(params.selector).slideDown(params.duration);
    },

    slideToggle:function(params) {
      $(params.selector).slideToggle(params.duration);
    },

    slideToggleAndRemove:function(params) {
      // we should actually use slideDown, but it doesn't work ?? TODO : INVESTIGATE AND FIX
      $(params.selector).slideToggle(params.duration, function(){$(this).remove()})
    },


    hilite:function (    params){ $(params.selector).addClass(   'hilite')          },

    addClass   :function(params){ $(params.selector).addClass(   params.className)  },
    toggleClass:function(params){ $(params.selector).toggleClass(params.className)  },
    removeClass:function(params){ $(params.selector).removeClass(params.className)  },

    disableLink  :function(params){ $(params.selector).removeAttr("href").fadeTo(100, .5); },
    disableButton:function(params){ $(params.selector).attr('disabled','disabled') },

    effect       :function(params){
        $(params.selector).effect(params.effect, params.options, params.speed)
    },

    //#TODO : make this parametrizable - ex: call('update_approved')
    call_update_approved :function(params){
        update_approved();
    },

    //#TODO : make this parametrizable - ex: call('update_approved')
    call_update_pendings:function(params){
        update_pendings();
    }
}


