String.prototype.trim = function() { return this.replace(/^\s*/, "").replace(/\s*$/, ""); }

// limit the characters in a textarea + display 'n characters left' edit
// length is obtained via the TEXTAREA maxlength attribute.
$(document).ready(function() {

 $('textarea.word_count').each(function(){

     var maxlimit = parseInt($(this).attr('maxlength'));

     // ...on page load
     var length = $(this).val().length;
     if(length >= maxlimit) {
       $(this).val($(this).val().substring(0, maxlimit));
       length = maxlimit;
      }
     $(this).parent().find('.counter').html( (maxlimit - length) + ' characters left');

      // on keyup events
     $(this).keyup(function(){
       var new_length = $(this).val().length;
       if(new_length >= maxlimit) {
         $(this).val($(this).val().substring(0, maxlimit));
         new_length = maxlimit;
        }

       var counterToUpdate = $(this).parent().find('.counter');
       counterToUpdate.html( (maxlimit - new_length) + ' characters left');
     });
 });

});


function init_reply_to(lnk){
    parent = $(lnk).parent('div.tweet')

    src_status_id   = parent.attr('data-status-id');
    src_screen_name = parent.attr('data-screen-name');

    $('#message_in_reply_to_status_id'  ).val(src_status_id  );
    $('#message_in_reply_to_screen_name').val(src_screen_name);

    msg = "@" + src_screen_name + " ";
    $("#create_message_body").val(msg);
    $("#create_message_body").focus();
}
function init_retweet(lnk){
    parent = $(lnk).parent('div.tweet')
    src_screen_name = parent.attr('screen_name');
    src_text        = $('.text', parent).text().trim() ;
    msg = "RT @" + src_screen_name + " : " + src_text;
    set_message_text_and_focus(msg);
}

function set_message_text_and_focus(text){
    $("#create_message_body").val(text);
    $("#create_message_body").focus();
}

function update_ui_after_cancelling_a_retweet(){
    $('#create_message_form'  ).show();
    $("#retweet_message").hide();
    $('.action a.edit').show();
}

function clear_create_form(){
    $('#create_message form input[type=text], #create_message form textarea').val('')
}



//------------------------------------------------------
// console#index
//------------------------------------------------------

function update_timeline(){
  var fetched_at = $('#statuses').attr('data-fetched_at');
  $.getScript('/console/update_timeline.js?last_fetched_at=' + fetched_at);
  setTimeout(update_timeline, 1000*APP_CONFIG.console.tl_autorefresh) ;
}

function update_approved(){
  var fetched_at = $('#approved').attr('data-fetched_at');
  $.getScript('/console/update_approved.js?last_fetched_at=' + fetched_at);
  setTimeout(update_approved, 1000*APP_CONFIG.console.ap_autorefresh) ;
}

function update_pendings(){
  $.getScript('/console/update_pendings.js');
}

//------------------------------------------------------
function init_websocket_stuff(){
  params = APP_CONFIG.broadcaster

  var url = "ws://" + params.host + ":" + params.port + "/websocket";
  ws  = new WebSocket(url);

  //ws.onclose    = function() { debug("browser: socket closed"); };
  //ws.onopen     = function() { debug("browser: connected..." ); };

  ws.onmessage  = function(evt) {
      var json    = evt.data ;
      if (evt.data==="heartbeat") {
        WebsocketMonitor.on_new_heartbeat();
        analyze_the_heartbeats()

      }
      else {
        var obj     = jQuery.parseJSON(json);
        var fn      = obj.fn;
        var params  = obj.params;

        RemoteJquery[fn](params) ;
      }
  };
}


//------------------------------------------------------

function start_listening_to_heartbeats(){
  setTimeout(analyze_the_heartbeats, 1000*APP_CONFIG.broadcaster.heartbeat_delay);
}


function signal_the_ws_broadcaster_is_down_and_reload_the_page() {
  msg = "Network Error (The Websocket Broadcaster did not respond)"
  msg = msg + "\n"
  msg = msg + "\nThe page will be reloaded"
  alert(msg)
  window.location = "/"
}


function analyze_the_heartbeats(){
  if (WebsocketMonitor.last_heartbeat_is_too_old() ) {
    signal_the_ws_broadcaster_is_down_and_reload_the_page();
  } else {
    var last_heartbeat = $('body').attr('last_heartbeat_at');
    var msg = "<br> last heartbeat received at:"+ last_heartbeat
    msg = msg + "<br> hb age : " + (WebsocketMonitor.last_heartbeat_age_ms()/1000) + ' sec.';
    $('#heartbeat_msg').html(msg)

    setTimeout(analyze_the_heartbeats, 1000*APP_CONFIG.broadcaster.heartbeat_delay);
  }
};



var WebsocketMonitor = {

  set_last_heartbeat:    function (value){ $('body').attr('last_heartbeat_at', value);           },
  get_last_heartbeat:    function ()     { return new Date($('body').attr('last_heartbeat_at')); },

  on_new_heartbeat: function (){
    this.set_last_heartbeat(new Date());
  },

  last_heartbeat_age_ms: function (){
    return new Date() - this.get_last_heartbeat();
  },

  last_heartbeat_is_too_old: function (){
    var max_delay = 1000*APP_CONFIG.broadcaster.max_heartbeat_delay
    return max_delay  < this.last_heartbeat_age_ms()
  }
}

