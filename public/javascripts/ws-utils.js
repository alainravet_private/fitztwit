String.prototype.startsWith = function(str) {return (this.match("^"+str)==str)}


$(document).ready(function(){
  function debug(str){ $("#debug").append("<p>"+str+"</p>"); };

    ws = new WebSocket("ws://localhost:8080/websocket");
    ws.onclose    = function() { debug("browser: socket closed"); };
    ws.onopen     = function() { debug("browser: connected..." ); };

    ws.onmessage  = function(evt) {
        var json    = evt.data ;
        var obj     = jQuery.parseJSON(json);
        var fn      = obj.fn;
        var params  = obj.params;

        if      ('remove'==fn) { fn_remove(params)      }
        else if ('hilite'==fn) { fn_hilite(params)      }
        else                   { console.log('66'+fn)   }

        switch(fn)
        {
            case 'toggle': FN.toggle(params) ; break;
            case 'hide'  : FN.hide  (params) ; break;
            case 'remove': FN.remove(params) ; break;

            case 'hilite': FN.hilite(params) ; break;

            case 'addClass'   : FN.addClass   (params) ; break;
            case 'toggleClass': FN.toggleClass(params) ; break;
            case 'removeClass': FN.removeClass(params) ; break;

            case 'disableLink'  : FN.disableLink  (params) ; break;
            case 'disableButton': FN.disableButton(params) ; break;

            default:
                console.log('invalid fn key: ' + fn);
        }
    };
});

var FN = {
    toggle:function(params){ $(params.selector).toggle()  },
    hide  :function(params){ $(params.selector).hide()    },
    remove:function(params){ $(params.selector).remove()  },


    hilite:function (params){
        //{"fn":"hilite","params":{"dom_id":"message_1"}}
        console.log('do hilite')
    },

    addClass   :function(params){ $(params.selector).addClass(   params.className)  },
    toggleClass:function(params){ $(params.selector).toggleClass(params.className)  },
    removeClass:function(params){ $(params.selector).removeClass(params.className)  },

    disableLink  :function(params){ $(params.selector).removeAttr("href").fadeTo(100, .5); },
    disableButton:function(params){ $(params.selector).attr('disabled','disabled') }

}
