require 'spec_helper'

describe MessagesController do

  let(:the_author ){ Factory(:user, :login => 'user_1')         }
  before { login_as(the_author )}


  context "when the WS broadcaster is down" do
    before { install_fake_broadcast_server(false) }
    example "XHR POST create updates the UIs via websocket" do
      xhr :post, :create, :message=> {:body => "test-1-2"}
    end

    example "XHR POST approve updates the UIs via websocket" do
      @message = create_a_pending_message
      approver = login_as_another_user
      xhr :put, :approve, :id => @message
    end

  end
  
end
