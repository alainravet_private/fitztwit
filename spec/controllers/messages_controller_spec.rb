#
# Note: this controller will only be used via AJAX -> no html view rendering, no redirect, etc..
#
#TODO : check with anonymous user (-> 502)

require 'spec_helper'

describe MessagesController do
  before { install_fake_broadcast_server }
  before { login_as(@user_1 = Factory(:user)) }

  #------------------------------------------------------------------------------------
  describe "GET index" do
  #------------------------------------------------------------------------------------
    it "assigns all messages as @messages" do
      @message = Factory(:message)
      Message.stubs(:all).returns([@message])
      get :index
      assigns(:messages).should eq([@message])
    end
  end

  #------------------------------------------------------------------------------------
  describe "XHR POST create" do
  #------------------------------------------------------------------------------------

    before do
      xhr :post, :create, :message=> {:body => "test-1-2"}
    end

    it {should render_template 'messages/create' }
    it 'should create the record' do
      @user_1.should have(1).message
      @user_1.messages.collect(&:body).should == ["test-1-2"]
    end
  end


  example "XHR POST create updates the UIs via websocket" do
    ws = sequence('websockets')
    RemoteJquery.any_instance.expects(:rem_send).with('call_update_pendings'                     ).in_sequence(ws)
    RemoteJquery.any_instance.expects(:rem_send).with('effect', has_entry(:effect => 'highlight')).in_sequence(ws)

    xhr :post, :create, :message=> {:body => "test-1-2"}
  end


  #------------------------------------------------------------------------------
  describe "XHR POST create_retweet_from " do
  #------------------------------------------------------------------------------
    before do
      @status = Factory(:status, :content => 'a-content', :status_id => 54321)
      Status.expects(:find_by_status_id).with(@status.id.to_s).returns(@status)
      @counts_before = counts_for(Message)
    end

    before { xhr :post, :create_retweet_from, :status_id => @status.to_param}


    it 'inits @message as a retweet-message' do
      m = assigns[:message]
      m                 .should be_retweet
      m.retweet_source  .should == @status
      m.body            .should == @status.content
    end
    it {should render_template 'create_retweet_from'}

    it "creates 1 new message" do
      Message.count.should == 1 + @counts_before[Message]
    end
  end

  example "XHR POST create_retweet_from updates the UIs via websocket" do
    @status = Factory(:status, :content => 'a-content', :status_id => 54321)
    Status.expects(:find_by_status_id).with(@status.id.to_s).returns(@status)

    ws = sequence('websockets')
    RemoteJquery.any_instance.expects(:rem_send).with('call_update_pendings'                     ).in_sequence(ws)
    RemoteJquery.any_instance.expects(:rem_send).with('effect', has_entry(:effect => 'highlight')).in_sequence(ws)

    xhr :post, :create_retweet_from, :status_id => @status.to_param
  end


  #------------------------------------------------------------------------------------
  describe "XHR PUT approve" do
  #------------------------------------------------------------------------------------
    before do
      @message = create_a_pending_message
      approver = login_as_another_user

      xhr :put, :approve, :id => @message
      @message.reload
    end

    it('approves the message') { @message.should be_approved }

    example "a message author cannot approve his own message"
  end


  example "XHR POST approve updates the UIs via websocket" do
    @message = create_a_pending_message
    approver = login_as_another_user

    ws = sequence('websockets')
    RemoteJquery.any_instance.expects(:rem_send).with('remove').in_sequence(ws)
    RemoteJquery.any_instance.expects(:rem_send).with('call_update_approved').in_sequence(ws)

    xhr :put, :approve, :id => @message
  end


  #------------------------------------------------------------------------------------
  describe "XHR PUT start_editing" do
  #------------------------------------------------------------------------------------
    before do
      @message = create_a_pending_message
      editor   = login_as_another_user

      xhr :put, :start_editing, :id => @message
      @message.reload
    end

    it {should render_template 'messages/start_editing' }
    it('update the message state') { @message.should be_being_edited }
  end


  example "XHR POST start_editing updates the UIs via websocket" do
    @message = create_a_pending_message
    editor   = login_as_another_user

    ws = sequence('websockets')
    RemoteJquery.any_instance.expects(:rem_send).with('html', any_parameters).in_sequence(ws)
    RemoteJquery.any_instance.expects(:rem_send).with('effect', has_entry(:effect => 'highlight')).in_sequence(ws)

    xhr :put, :start_editing, :id => @message
  end


  #------------------------------------------------------------------------------------
  describe "XHR PUT end_editing" do
  #------------------------------------------------------------------------------------
    before do
      @message          = create_a_pending_message
      @original_author  = @message.author

      @editor           = login_as_another_user
      @message.start_editing
    end


    context ", with changes to the message" do
  #------------------------------------------------------------------------------------
      before do
        xhr :put, :end_editing, :id => @message,
            :message => {:body => (@new_body= Time.now.to_s)}
        @message.reload
      end

      it {should render_template 'messages/end_editing' }
      it('update the message state') { @message.should be_pending }
      it('update the message body' ) { @message.body.should == @new_body}
    end


    example ", with changes to the message updates the UIs via websocket" do
      ws = sequence('websockets')
      RemoteJquery.any_instance.expects(:rem_send).with('html', any_parameters).in_sequence(ws)
      RemoteJquery.any_instance.expects(:rem_send).with('effect', has_entry(:effect => 'highlight')).in_sequence(ws)

      xhr :put, :end_editing, :id => @message,
          :message => {:body => (@new_body= Time.now.to_s)}
    end


    context ", with no changes" do
  #------------------------------------------------------------------------------------
      before do
        xhr :put, :end_editing, :id => @message,
            :message => {:body => @message.body}
        @message.reload
      end

      it {should render_template 'messages/end_editing' }
      it('update the message state back') { @message.should be_pending }
      it('does not change the author'   ) { @message.author.should == @original_author}
    end


    context ", with no params[:message](== cancel)" do
  #------------------------------------------------------------------------------------
     before do
       xhr :put, :end_editing, :id => @message
       @message.reload
     end

     it {should render_template 'messages/end_editing' }
     it('update the message state back') { @message.should be_pending }
     it('does not change the author'   ) { @message.author.should == @original_author}
   end
  end


  #------------------------------------------------------------------------------------
  describe "XHR DELETE destroy" do
  #------------------------------------------------------------------------------------
    before do
      @message = Factory(:message)
      xhr :delete, :destroy, :id => @message.id
    end

    it 'should delete the record' do
      Message.find_by_id(@message.id).should_not be
    end
  end

  example "XHR DELETE destroy updates the UIs via websocket" do
    ws = sequence('websockets')
    RemoteJquery.any_instance.expects(:rem_send).with('slideToggleAndRemove', any_parameters).in_sequence(ws)

    @message = Factory(:message)
    xhr :delete, :destroy, :id => @message.id
  end
end
