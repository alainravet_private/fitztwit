require 'spec_helper'

describe MessagesController do
  before { install_fake_broadcast_server }
  before { login_as(the_author )}
  before do
    Message.stubs(:find).with("37").returns(the_message)
  end

  let(:the_author ){ Factory(:user, :login => 'user_1')         }
  let(:the_message){ Factory(:message, :created_by => the_author.id) }

#--------------------------------------------------------------------------------------------
  describe "XHR DELETE destroy " do
#--------------------------------------------------------------------------------------------
    after { xhr :delete, :destroy, :id => '37' }

    example "on a destroyable? object" do
      the_message.expects(:destroyable?).at_least_once.returns(true)
      the_message.expects(:destroy)
    end

    example "on a NON destroyable? object" do
      the_message.expects(:destroyable?).at_least_once.returns(false)
      the_message.expects(:destroy).never
    end
  end


  describe "XHR DELETE destroy a non-destroyable? message" do
#------------------------------------------------------------------------------
    before do
      @message          = create_a_pending_message
      @message.expects(:destroyable?).at_least_once.returns(false)
      Message.stubs(:find).with(@message.id.to_s).returns(@message)
    end
    before { @message.expects(:destroy).never }
    before { xhr :delete, :destroy, :id => @message.to_param}


    it 'show the proper error message' do
      should render_template '_shared/_signal_error'
      assigns[:signaled_error_msg].should == I18n.t("console.error.cannot_destroy_an_undestroyable_message")
    end
  end



#--------------------------------------------------------------------------------------------
  describe "XHR PUT approve" do
#--------------------------------------------------------------------------------------------
    after { xhr :put, :approve, :id => '37' }

    example "on an approvable? object" do
      the_message.expects(:approvable?).returns(true)
      the_message.expects(:approve)
      login_as_another_user
    end

    example "on a non-approvable? object" do
      login_as_another_user
      the_message.expects(:approvable?).returns(false)
      the_message.expects(:approve).never
    end
  end


  describe "XHR PUT approve as the message author" do
#------------------------------------------------------------------------------
    before do
      @message = create_a_pending_message
      @message.stubs(:approvable? => true)
      Message.stubs(:find).with(@message.id.to_s).returns(@message)
    end
    before { @message.expects(:approve).never }
    before { xhr :put, :approve, :id => @message.to_param }


    it 'show the proper error message' do
      should render_template '_shared/_signal_error'
      assigns[:signaled_error_msg].should == I18n.t("console.error.author_cannot_approve_his_own_message")
    end
  end

  describe "XHR PUT approve a non-approvable message" do
#------------------------------------------------------------------------------
    before do
      @message = create_a_pending_message
      @message.stubs(:approvable? => false)
      Message.stubs(:find).with(@message.id.to_s).returns(@message)
    end
    before { @message.expects(:approve).never }
    before { xhr :put, :approve, :id => @message.to_param }


    it 'show the proper error message' do
      should render_template '_shared/_signal_error'
      assigns[:signaled_error_msg].should == I18n.t("console.error.cannot_approve_non_approvable_message")
    end
  end


#--------------------------------------------------------------------------------------------
  describe "XHR PUT start_editing" do
#--------------------------------------------------------------------------------------------
    after { xhr :put, :start_editing, :id => '37' }

    example "on an approvable? object" do
      the_message.expects(:editable?).returns(true)
      the_message.expects(:start_editing)
    end

    example "on a non-approvable? object" do
      the_message.expects(:editable?).returns(false)
      the_message.expects(:start_editing).never
    end
  end

  describe "XHR PUT start_editing a non-editable message" do
#------------------------------------------------------------------------------
    before do
      @message          = create_a_pending_message
      @message.expects(:editable?).returns(false)
      Message.stubs(:find).with(@message.id.to_s).returns(@message)
    end
    before { @message.expects(:start_editing).never }
    before { xhr :put, :start_editing, :id => @message.to_param}


    it 'shows the proper error message' do
      should render_template '_shared/_signal_error'
      assigns[:signaled_error_msg].should == I18n.t("console.error.cannot_edit_an_uneditable_message")
    end
  end



#--------------------------------------------------------------------------------------------
  describe "XHR PUT end_editing" do
#--------------------------------------------------------------------------------------------
    after do
      the_message.stubs(:editor).returns(User.current)
      xhr :put, :end_editing, :id => '37', :message => {:body => Time.now.to_s}
    end

    example "on a 'being_edited' object" do
      the_message.expects(:being_edited?).returns(true)
      the_message.expects(:end_editing)
    end

    example "on a non-being_edited? object" do
      the_message.expects(:being_edited?).returns(false)
      the_message.expects(:end_editing).never
    end
  end

  describe "XHR PUT end_editing not as the message editor" do
#------------------------------------------------------------------------------
    before do
      @message          = create_a_pending_message
      @editor           = login_as_another_user
      @message.start_editing

      user_3 = login_as_another_user

      Message.stubs(:find).with(@message.id.to_s).returns(@message)
    end
    before { @message.expects(:end_editing).never }
    before { xhr :put, :end_editing, :id => @message.to_param, :message => {:body => Time.now.to_s} }


    it 'show the proper error message' do
      should render_template '_shared/_signal_error'
      assigns[:signaled_error_msg].should == I18n.t("console.error.only_editor_can_complete_edition")
    end
  end


  describe "XHR PUT end_editing a message that is not being_edited?" do
#------------------------------------------------------------------------------
    before do
      @message          = create_a_pending_message
      non_editor        = login_as_another_user
      @message.expects(:being_edited?).returns(false)
      Message.stubs(:find).with(@message.id.to_s).returns(@message)
    end
    before { @message.expects(:end_editing).never }
    before { xhr :put, :end_editing, :id => @message.to_param, :message => {:body => Time.now.to_s} }


    it 'show the proper error message' do
      should render_template '_shared/_signal_error'
      assigns[:signaled_error_msg].should == I18n.t("console.error.cannot_update_a_not_being_edited_message")
    end
  end

end
