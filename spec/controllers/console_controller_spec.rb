require 'spec_helper'

describe ConsoleController do
  #TODO : check with anonymous user (-> 502)
  before { login_as(@user = Factory(:user)) }

  describe "GET 'index'" do
    it "should be successful" do
      get 'index'
      response.should be_success
    end
  end


  describe "GET 'index' when the user is the editor of 1 message" do
    before do
      Factory(:user, :login => 'the-author').make_current
      @message = Factory(:message)

      login_as(@editor = Factory(:user, :login => 'the-editor'))
      @message.start_editing
    end

    it "should populate @being_edited_message" do
      get 'index'
      assigns[:being_edited_message].try(:id).should == @message.id
    end
  end

end
