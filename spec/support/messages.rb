def mock_message(stubs={})
  @mock_message ||= mock_model(Message, stubs).as_null_object
end


def create_a_pending_message
  Factory(:message)
end

def rand_tweet(prefix='rand_tweet', seed=nil)
  rand_v = seed ?
      Random.new(seed).rand(1000000) :
      rand(1000000)
  time_v = seed ?
      nil :
      Time.now.to_s
  "#{prefix} + #{time_v} + #{rand_v}"
end

def rand_tweet(prefix='rand_tweet', seed=nil)
  rand_v = rand(1000000)
  time_v = seed ?
      nil :
      Time.now.to_s
  "#{prefix} + #{time_v} + #{rand_v}"
end
