
def login_as(user)
  user.make_current
  session[:user_id] = user.id
end

def login_as_another_user
  Factory(:user, :login => 'another-user').tap do |user|
    login_as(user)
  end
end


