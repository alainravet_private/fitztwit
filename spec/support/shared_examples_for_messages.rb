module Spec
  module Rails
    module Example

      class ModelExampleGroup

        #-------------------------------------------------------------------------------
        shared_examples_for 'a pending message' do
        #-------------------------------------------------------------------------------
          its(:state){ should == Message::State::PENDING }
          it {should be_pending}

          describe "for his author" do
            before() { subject.author.make_current }
            it('is not approvable?  ') { subject.should_not be_approvable  }
            it('is editable?'        ) { subject.should     be_editable    }
            it('is not updatable?'   ) { subject.should_not be_updatable   }
          end

          describe "for another user" do
            before() { Factory(:user).make_current }
            it('is approvable?'     ) { subject.should be_approvable }
            it('is editable?'       ) { subject.should be_editable   }
            it('is not updatable?'  ) { subject.should_not be_updatable   }
          end
          it('is is not tweetable? ') { subject.should_not be_tweetable   }
        end



        #-------------------------------------------------------------------------------
        shared_examples_for 'a non-editable pending message' do
        #-------------------------------------------------------------------------------
          its(:state){ should == Message::State::PENDING }
          it {should be_pending}

          describe "for his author" do
            before() { subject.author.make_current }
            it('is not approvable?  ' ) { subject.should_not be_approvable  }
            it('is not editable?'     ) { subject.should_not be_editable    }
            it('is not updatable?'    ) { subject.should_not be_updatable   }
          end

          describe "for another user" do
            before() { Factory(:user).make_current }
            it('is approvable?'       ) { subject.should     be_approvable  }
            it('is not editable?'     ) { subject.should_not be_editable    }
            it('is not updatable?'    ) { subject.should_not be_updatable   }
          end
          it('is is not tweetable? ') { subject.should_not be_tweetable   }
        end
      end

      class ControllerExampleGroup
      end

    end
  end
end
