# counts AR objects and returns the results in a hash with the AR classes as keys.
#
# Example :
# <code>
#   example('foo.destroy should destroy the object and its 2 related bars) do
#     @foo = Foo.create_with_2_bars!
#     @counts_before = counts_for(Foo, Bar)         # -> {Foo => 1, Bar => 2}
#
#     @foo.destroy
#
#     Foo.count.should == @counts_before[Foo] -1    # <-- READ
#     Bar.count.should == @counts_before[Bar] -2    # <-- READ
#   end
# </code>

def counts_for(*classes)
  # we use HashWithAutoStringKeys instead of Hash so that puts *.inspect is more readable
  HashWithAutoStringKeys.new.tap do |result|
    classes.each { |c| result[c.to_s] = c.count }
  end
end
