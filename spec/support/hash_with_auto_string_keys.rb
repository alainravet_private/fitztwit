# Variation of Hash where the keys are always stringified before being used a key
# <code>
# Examples :
#   h[Operator]       <==>   h[Operator.to_s]
#   h[Operator] = 2   <==>   h[Operator.to_s] = 2
#</code>

class HashWithAutoStringKeys < Hash
  alias_method :regular_reader, :[] unless method_defined?(:regular_reader)
  alias_method :regular_writer, :[]= unless method_defined?(:regular_writer)

  #
  # <code>h[Operator] <==> h[Operator.to_s]
  def [](key)
    regular_reader(key.to_s)
  end

  # <code>h[Operator] = 2  <==> h[Operator.to_s] = 2
  def []=(key, value)
    regular_writer(key.to_s, value)
  end
end
