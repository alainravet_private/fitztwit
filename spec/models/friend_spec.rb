require 'spec_helper'

describe Friend do
  it { should validate_presence_of(:screen_name) }
  it { should validate_presence_of(:name       ) }

  it { should have_many(:statuses) }
end
