require 'spec_helper'

describe Status do
  it { should validate_presence_of(:status_id         ) }
  it { should validate_presence_of(:status_created_at ) }
  it { should validate_presence_of(:source            ) }
  it { should validate_presence_of(:screen_name       ) }
  it { should validate_presence_of(:name              ) }
  it { should validate_presence_of(:profile_image_url ) }
  it { should validate_presence_of(:content           ) }
  it { should validate_presence_of(:name              ) }

  it { should belong_to(:friend) }
end
