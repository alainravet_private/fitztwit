require 'spec_helper'

FROZEN_1_WEEK_AGO = 1.week.ago # for Timecoping
FROZEN_NOW        = 1.hour.ago
FROZEN_LATER      = 1.hour.from_now

describe Message do


  #-------------------------------------------------------------------------------
  describe ".create()" do
  #-------------------------------------------------------------------------------
    before() { Factory(:user, :login => 'the-author').make_current }
    before() do
      @message = Factory(:message)
    end
    subject{@message}

    it { should validate_presence_of(:created_by) }
    it { should validate_presence_of(:body      ) }
    it { should validate_presence_of(:state     ) }

    it { should belong_to(:author) }

    its(:author) { should == User.current}

    it_should_behave_like 'a pending message'
  end


  #-------------------------------------------------------------------------------
  describe ".start_editing()" do
  #-------------------------------------------------------------------------------
    before() { Factory(:user, :login => 'the-author').make_current }
    before() { @message = Factory(:message) }
    before() do
      @editor = Factory(:user, :login => 'the-editor').make_current
      Timecop.freeze(FROZEN_NOW) do
        @message.start_editing
      end
    end
    subject{@message}


    its(:state){ should == Message::State::BEING_EDITED }
    it {should be_being_edited}

    its(:editor         ){should == @editor}
    its(:being_edited_at){should == FROZEN_NOW}

    describe "for his original author" do
      before() { subject.author.make_current }
      it('is not approvable?  ') { subject.should_not be_approvable  }
      it('is not editable?'    ) { subject.should_not be_editable    }
      it('is not updatable?'   ) { subject.should_not be_updatable   }
      it('is not tweetable? '  ) { subject.should_not be_tweetable   }
    end

    describe "for another user" do
      before() { Factory(:user).make_current }
      it('is not approvable?'     ) { subject.should_not be_approvable  }
      it('is not editable?'       ) { subject.should_not be_editable    }
      it('is not updatable?'   ) { subject.should_not be_updatable   }
      it('is not tweetable?'      ) { subject.should_not be_tweetable   }
    end

    describe "for the editor" do
      before() { @editor.make_current }
      it('is not approvable?'     ) { subject.should_not be_approvable  }
      it('is not editable?'       ) { subject.should_not be_editable    }
      it('is     updatable?'      ) { subject.should     be_updatable   }
    end
  end



  #-------------------------------------------------------------------------------
  describe ".end_editing()" do
  #-------------------------------------------------------------------------------
    before() do
      @original_author = Factory(:user, :login => 'the-author').make_current
      @message = Factory(:message)
    end
    before() do
      @editor   = Factory(:user, :login => 'the-editor').make_current
      @message.start_editing
      @message.reload
    end


    #-------------------------------------------------------------------------------
    context "after the message was changed" do
    #-------------------------------------------------------------------------------
      before() do
        @new_body     = @message.body + Time.now.to_s
        @message.body = @new_body
        @message.end_editing
      end
      subject{@message.reload}

      it("should have the editor as the new author") { subject.author.should == @editor }
      it("saves the modified text"                 ) { subject.body  .should == @new_body }

      it_should_behave_like 'a pending message'
    end


    #-------------------------------------------------------------------------------
    context ".end_editing() after the message was NOT changed" do
    #-------------------------------------------------------------------------------
      before() do
        #no message's changes here !
        @message.end_editing
      end
      subject{@message.reload}

      it("should keep the original author as its author") { subject.author.should == @original_author }

      it_should_behave_like 'a pending message'
    end

  end

end
