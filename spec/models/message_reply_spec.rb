require 'spec_helper'

describe 'Message reply' do
  before() { Factory(:user, :login => 'the-author').make_current }
  before() do
    @status  = Factory(:status, :content => 'original-content')
    @message = Factory(:message, :in_reply_to => @status)
  end
  subject{@message}


  it { should be_valid   }
  it 'is invalid if only the in_reply_to_status_screen_name is known' do
    @message.in_reply_to_status_id=nil
    @message.should be_invalid
  end
  it 'is invalid if in_reply_to_status_id is set and in_reply_to_screen_name is nil' do
    @message.in_reply_to_screen_name = nil
    @message.should be_invalid
  end
  it 'is invalid if in_reply_to_status_id is set and in_reply_to_screen_name is blank' do
    @message.in_reply_to_screen_name = ' '
    @message.should be_invalid
  end

  it 'is valid if in_reply_to_status_id is nil and in_reply_to_screen_name is blank' do
    @message.in_reply_to_screen_name = ' '
    @message.in_reply_to_status_id   = nil
    @message.should be_valid
  end

  its(:in_reply_to_screen_name){should == @status.screen_name}
  its(:in_reply_to_status_id  ){should == @status.status_id  }
  it 'links to the reply source' do
    subject.in_reply_to.should == @status
  end

  it { should be_reply}

  it_should_behave_like 'a pending message'

end
