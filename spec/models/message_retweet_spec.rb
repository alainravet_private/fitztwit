require 'spec_helper'

describe 'Message retweet' do
  before() { Factory(:user, :login => 'the-author').make_current }
  before() do
    @status  = Factory(:status, :content => 'original-content')
    @message = Factory(:message, :retweet_source => @status)
  end
  subject{@message}


  it { should be_valid   }
  it 'is invalid if only the retweet_source_status_id is known' do
    @message.retweet_source_status_id=nil
    @message.should be_invalid
  end
  it 'is invalid if only the retweet_source_id is known' do
    @message.retweet_source_id = nil
    @message.should be_invalid
  end

  its(:retweet_source_id        ){should == @status.id}
  its(:retweet_source_status_id ){should == @status.status_id}
  it 'links to the retweet source' do
    subject.retweet_source.should == @status
  end

  it { should be_retweet }

  it_should_behave_like 'a non-editable pending message'


  example ".start_editing raises a Message::InvalidOperationError" do
    expect {
      subject.start_editing
    }.to raise_error(Message::InvalidOperationError)
  end

  example ".end_editing raises a Message::InvalidOperationError" do
    expect {
      subject.end_editing
    }.to raise_error(Message::InvalidOperationError)
  end
end
