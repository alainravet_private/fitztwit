require 'spec_helper'

describe Message do
  before(:all) do
    Factory(:user, :login => 'the_current_user').make_current
  end

  subject{Factory(:message)}

#-------------------------------------------------------------------------------

  Message::State::ALL.each do |state|
    it { should allow_value(state).for(:state) }
  end

#-------------------------------------------------------------------------------

  describe "by default" do
    its(:state) { should     == Message::State::PENDING}
    it { should     be_pending    }
    it { should_not be_approved   }
    it { should_not be_being_edited}
    it { should_not be_tweeted    }

    it { should_not   be_tweetable}
    it { should       be_destroyable}
    it { should       be_editable}
  end


#-------------------------------------------------------------------------------

  context '.approve()' do
    before() do
      @user_2 = Factory(:user, :login => 'a-2nd-user')
      @user_2.make_current
      subject.approve
    end

    its(:approved_by) {should == @user_2.id }
    its(:state      ) {should == Message::State::APPROVED }

    it { should_not   be_pending    }
    it { should       be_approved   }
    it { should_not   be_being_edited}
    it { should_not   be_tweeted    }

    it { should_not   be_approvable }
    it { should_not   be_editable   }
    it { should       be_tweetable  }
    it { should       be_destroyable}
  end


#-------------------------------------------------------------------------------

  context '.start_editing()' do
    before() do
      @user_3 = Factory(:user, :login => 'a-2nd-user').make_current
      subject.start_editing
    end

    its(:being_edited_by) {should == @user_3.id}
    its(:state     ) {should == Message::State::BEING_EDITED }

    it { should_not   be_pending    }
    it { should_not   be_approved   }
    it { should       be_being_edited}
    it { should_not   be_tweeted    }

    it { should_not   be_approvable }
    it { should_not   be_editable   }
    it { should_not   be_tweetable  }
    it { should_not   be_destroyable}
  end
end
