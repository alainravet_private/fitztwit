require "spec_helper"

describe ConsoleController do
  describe "routing" do


    it "recognizes the root and => console#index" do
      { :get => "/" }.should route_to(:controller => "console", :action => "index")
    end

    it "recognizes and generates #inject_fake_messages" do
      { :post => "/console/inject_fake_messages" }.should route_to(:controller => "console", :action => "inject_fake_messages")
    end

    it "recognizes and generates #pull_timeline" do
      { :post => "/console/pull_timeline" }.should route_to(:controller => "console", :action => "pull_timeline")
    end

    it "recognizes and generates #tweet_all_approved" do
      { :post => "/console/tweet_all_approved" }.should route_to(:controller => "console", :action => "tweet_all_approved")
    end
  end
end
