# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :friend do |f|
  f.screen_name "MyString"
  f.name "MyString"
  f.last_profile_image_url "MyString"
  f.last_tweet_at "2011-02-14 19:52:22"
end
