# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :message do |f|
  f.body "MyText"
  f.created_by 1
  f.created_at "2011-02-12 00:09:30"
end
