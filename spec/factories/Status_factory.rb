# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :status do |f|
  f.status_id 1234567890
  f.screen_name "MyString"
  f.name "MyString"
  f.content "MyString"
  f.status_created_at "2011-02-14 19:41:11"
  f.profile_image_url "MyString"
  f.source "MyString"
end
