require 'spec_helper'


describe StatusUpdater do

  before { Factory(:user).make_current }

  #TODO : move those value to a test-dedicated config file
  A_STATUS_ID_OF_OURS     = 38654868180840448   # for the ''d3d'        account

  IN_REPLY_TO_SCREEN_NAME = 'alainravet2'       # for the 'alainravet2' account
  IN_REPLY_TO_STATUS_ID   = 37278815264702465   # for the 'alainravet2' account


  #--------------------------------------------------------------------------------------------------
  describe "reply to 2 statuses (1 public + 1 of ours" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/reply_to_2_statuses_incl_1_of_ours_200", :record => :new_episodes

    before do
      m1 = Factory(:message, :in_reply_to => Factory(:status, :status_id => A_STATUS_ID_OF_OURS  ), :body => rand_tweet('test reply#3', 1))
      m2 = Factory(:message, :in_reply_to => Factory(:status, :status_id => IN_REPLY_TO_STATUS_ID), :body => rand_tweet('test reply#4', 2))
      @result = StatusUpdater.tweet_all([m1, m2])
    end

    it("succeeds") do
      @result.should == :ok
    end
  end


  #--------------------------------------------------------------------------------------------------
  describe ".tweet_a_reply()" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/fn_tweet_a_reply_200", :record => :new_episodes

    before do
      @status = Factory(:status, :status_id => IN_REPLY_TO_STATUS_ID)
      @m      = Factory(:message, :in_reply_to => @status, :body => rand_tweet("@#{IN_REPLY_TO_SCREEN_NAME} test replyto", 3))

      @result = StatusUpdater.tweet_a_reply(@m)
    end

    it("succeeds") do
      @result.first.should == :ok
    end

    it "twitter linked this reply to its target" do
      details = @result.last
      details.in_reply_to_screen_name.should == IN_REPLY_TO_SCREEN_NAME
      details.in_reply_to_status_id  .should == IN_REPLY_TO_STATUS_ID
    end
  end


end
