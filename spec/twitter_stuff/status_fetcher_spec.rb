# encoding: utf-8

require 'spec_helper'

describe StatusFetcher do

#--------------------------------------------------------------------------------------------------
  context "import_user_timeline" do
#--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/user_timeline_with_options", :record => :new_episodes

    before() do
      @counts_before = counts_for(Friend, Status)
      StatusFetcher.import_user_timeline
    end

    it("creates 1 Friend"  ) { Friend.count.should == 1 + @counts_before[Friend] }
    it("creates 8 Statuses") { Status.count.should == 8 + @counts_before[Status] }
    it "links the Friend to its new Statuses" do
      Friend.last.statuses.should == Status.limit(8).all
    end

    describe "the new statuses" do
      it 'are linked to the new Friend' do
        Status.limit(8).collect(&:friend_id).uniq.should == [Friend.last.id]
      end

      it 'source is UserTimeLine' do
        Status.limit(3).collect(&:source).uniq.should == [Status::Source::USER_TIMELINE]
      end
    end
    it "creates Statuses with the proper screen_name & content" do
      Status.all.collect(&:status_pair).should ==  [["d3d", "@davenolan AND NOW I AM REPLYING"],
                                                    ["d3d", "this is no longer a reply"],
                                                    ["d3d", "@alainravet2 this is a reply"],
                                                    ["d3d", "THIS IS a simple message"],
                                                    ["d3d", "DO NOT DELETE THIS STATUS - USED BY THE FITZTWIT APP TEST"],
                                                    ["d3d", "@alainravet2, third is for you"],
                                                    ["d3d", "second"],
                                                    ["d3d", "first"]]


    end

    it "doesn't create dupes when called twice with no new data" do
      @counts_before = counts_for(Friend, Status)
      StatusFetcher.import_user_timeline
      Friend.count.should == @counts_before[Friend]
      Status.count.should == @counts_before[Status]
    end
  end


#--------------------------------------------------------------------------------------------------
  context "import_friends_timeline" do
#--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/friends_timeline_with_options", :record => :new_episodes

    before() do
      @counts_before = counts_for(Friend, Status)
      StatusFetcher.import_friends_timeline
    end

    it("creates 1 Friend"  ) { Friend.count.should == 1 + @counts_before[Friend] }
    it("creates 4 Statuses") { Status.count.should == 4 + @counts_before[Status] }


    describe "the new Statuses" do
      it 'are linked to the new Friends' do
        expected_sn = %w(d3d d3d d3d d3d)
        Status.limit(4).collect(&:screen_name)                  .should == expected_sn
        Status.limit(4).collect(&:friend).collect(&:screen_name).should == expected_sn
      end

      it 'source is FriendsTimeLine' do
        Status.limit(7).collect(&:source).uniq.should == [Status::Source::FRIENDS_TIMELINE]
      end
    end

    it "creates Statuses with the proper screen_name & content" do
      Status.all.collect(&:status_pair).should ==
                [["d3d", "@davenolan AND NOW I AM REPLYING"],
                 ["d3d", "this is no longer a reply"],
                 ["d3d", "@alainravet2 this is a reply"],
                 ["d3d", "THIS IS a simple message"]]

    end

    it "doesn't create dupes when called twice with no new data" do
      @counts_before = counts_for(Friend, Status)
      StatusFetcher.import_friends_timeline
      Friend.count.should == @counts_before[Friend]
      Status.count.should == @counts_before[Status]
    end
  end



#--------------------------------------------------------------------------------------------------
  context "import_mentions" do
#--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/mentions_with_options", :record => :new_episodes

    before() do
      @counts_before = counts_for(Friend, Status)
      StatusFetcher.import_mentions
    end

    it("creates 3 Friend"  ) { Friend.count.should ==  3 + @counts_before[Friend] }
    it("creates 6 Statuses") { Status.count.should ==  6 + @counts_before[Status] }


    describe "the new Statuses" do
      it 'are linked to the new Friends' do
        expected_sn = %w(davenolan ichavolcova alainravet2 alainravet2 alainravet2 alainravet2)
        Status.limit(6).collect(&:screen_name)                  .should == expected_sn
        Status.limit(6).collect(&:friend).collect(&:screen_name).should == expected_sn
      end

      it 'source is FriendsTimeLine' do
        Status.limit(2).collect(&:source).uniq.should == [Status::Source::MENTIONS]
      end
    end

    it "creates Statuses with the proper screen_name & content" do
      all_pairs = Status.all.collect(&:status_pair)
      all_pairs[1][1] = "XXX" # quick workaround (keeps failing because it contains strange characters)
      all_pairs.should ==
                [["davenolan", "@d3d more coffee please"],
                 ["ichavolcova", "XXX"],
                 ["alainravet2", "@d3d test rake twitter:import_timeline_changes"],
                 ["alainravet2", "@d3d test this is a very long message this is a very long message this is a very long message this is a very long message this is a very lon"],
                 ["alainravet2", "this is for @d3d"],
                 ["alainravet2", "@d3d thank you"]]


    end

    it "doesn't create dupes when called twice with no new data" do
      @counts_before = counts_for(Friend, Status)
      StatusFetcher.import_mentions
      Friend.count.should == @counts_before[Friend]
      Status.count.should == @counts_before[Status]
    end
  end

end
