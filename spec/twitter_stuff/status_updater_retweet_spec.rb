require 'spec_helper'


describe StatusUpdater do

  before { Factory(:user).make_current }

  A_STATUS_ID_OF_OURS   = '38654868180840448' # for the ''d3d'    account
  A_STATUS_ID_OF_THEIRS = '37963546461474816' # for the 'twitter' account

  #--------------------------------------------------------------------------------------------------
  describe "retweet 1 of our own statuses" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/retweet_1_status_of_ours_403", :record => :new_episodes

    before do
      s = Factory(:status, :status_id => A_STATUS_ID_OF_OURS)
      m = Factory(:message, :retweet_source => s)
      @result = StatusUpdater.tweet_all([m])
    end

    it("fails") do
      @result.should == [[:retweet_failure, 403, I18n.t("twitter.error_messages.retweet_failure")]]
    end
  end

  #--------------------------------------------------------------------------------------------------
  describe "retweet 1 status of the public twitter account" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/retweet_1_status_of_theirs_200", :record => :new_episodes

    before do
      s = Factory(:status, :status_id => A_STATUS_ID_OF_THEIRS)
      m = Factory(:message, :retweet_source => s)
      @result = StatusUpdater.tweet_all([m])
    end

    it("fails") do
      @result.should == :ok
    end
  end

  #--------------------------------------------------------------------------------------------------
  describe "retweet 2 statuses (1 public + 1 of ours" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/retweet_2_statuses_incl_1_of_ours_403", :record => :new_episodes

    before do
      m1 = Factory(:message, :retweet_source => Factory(:status, :status_id => A_STATUS_ID_OF_OURS  ))
      m2 = Factory(:message, :retweet_source => Factory(:status, :status_id => A_STATUS_ID_OF_THEIRS))
      @result = StatusUpdater.tweet_all([m1, m2])
    end

    it("partially fails") do
      @result.should == [[:retweet_failure, 403, I18n.t("twitter.error_messages.retweet_failure")],
                         [:ok, 200]
                        ]
    end
  end

end