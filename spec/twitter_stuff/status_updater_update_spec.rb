require 'spec_helper'


describe StatusUpdater do
  let(:msg_1) { Message.create!(:body => (Time.now.to_s + rand(1000).to_s)) }
  let(:msg_2) { Message.create!(:body => (rand(1000).to_s) + Time.now.to_s) }
  let(:too_long_message) { Message.create!(:body => ('1'*150 + rand(1000).to_s + Time.now.to_s)) }

  before { Factory(:user).make_current }



  #--------------------------------------------------------------------------------------------------
  describe "send 1 tweet" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/tweet_1_message_200", :record => :new_episodes

    before { @result = StatusUpdater.tweet_all([msg_1]) }

    it("succeeds") { @result.should == :ok }
    it('marks the message as "tweeted"') { msg_1.reload.should be_tweeted }
  end


  #--------------------------------------------------------------------------------------------------
  describe "send 2 distinct tweets" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/tweet_2_messages_200", :record => :new_episodes

    before { @result = StatusUpdater.tweet_all([msg_1, msg_2]) }

    it("succeeds") { @result.should == :ok }
    it 'marks the 2 message as "tweeted"' do
      msg_1.reload.should be_tweeted
      msg_2.reload.should be_tweeted
    end
  end

  #--------------------------------------------------------------------------------------------------
  describe "send 1 tweet twice" do
  #--------------------------------------------------------------------------------------------------
    use_vcr_cassette "twitter/tweet_1_message_twice_403", :record => :new_episodes

    before { @result = StatusUpdater.tweet_all([msg_1, msg_1]) }

    it("has 1 success and 1 failure") do
      @result.should == [[:ok, 200],
                         [:update_failure, 403, I18n.t('twitter.error_messages.duplicate_status')]
      ]
    end
    it('DOES mark the message as "tweeted"') { msg_1.reload.should be_tweeted }
  end


  #--------------------------------------------------------------------------------------------------
  describe "send 1 tweet of 160+ characters" do
  #--------------------------------------------------------------------------------------------------

    use_vcr_cassette "twitter/tweet_1_message_too_long_403", :record => :new_episodes

    before { @result = StatusUpdater.tweet_all([too_long_message]) }

    it("fails") do
      @result.should == [[:update_failure, 403, I18n.t('twitter.error_messages.too_long_status')]]
    end
    it('does NOT mark the message as "tweeted"') { too_long_message.reload.should_not be_tweeted }
  end


end