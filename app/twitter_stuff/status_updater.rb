class StatusUpdater
  extend GrackleUtils

  def self.tweet_all_approved_messages
    StatusUpdater.tweet_all(Message.approved)
  end

  def self.tweet_all(messages)
    results = messages.collect do |m|
      tweet_one(m)
    end
    results.all?{|r, status, details| r==:ok} ?
            :ok :
            results
  end


private

  def self.tweet_one(m)
    result = m.retweet? ? retweet(m) :
             m.reply?   ? tweet_a_reply(m) :
                          tweet(m)
    m.mark_as_tweeted if :ok == result.first
    result
  end


  def self.tweet(m)
    begin
      with_retry do
        Tweet.client.statuses.update! :status=> m.body
      end
      [:ok, 200]
    rescue Grackle::TwitterError => e
      [:update_failure, e.status, e.response_object.error] #ex: [:update_failure, 403, "Status is a duplicate."
    end
  end



  def self.tweet_a_reply(m)
    begin
      with_retry do
        @result = Tweet.client.statuses.update! :status      => m.body,
                                      :in_reply_to_status_id => m.in_reply_to_status_id
      end
      [:ok, 200, @result]
    rescue Grackle::TwitterError => e
      [:reply_to_failure, e.status, e.response_object.error] #ex: [:update_failure, 403, "Status is a duplicate."
    end
  end



  def self.retweet(m)
    with_retry do
      Tweet.client.statuses.retweet! :id=> m.retweet_source_status_id
    end
    [:ok, 200]
  rescue Grackle::TwitterError => e
    errors = e.response_object.errors.
            split("\n").uniq.   #remove triplicates caused by 'with_retry'
            join("\n")
    [:retweet_failure, e.status, errors.inspect]

  end
end