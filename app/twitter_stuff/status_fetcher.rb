class StatusFetcher
  extend GrackleUtils

  def self.import_all
    StatusFetcher.import_friends_timeline
    StatusFetcher.import_mentions
    StatusFetcher.import_user_timeline
  end

  def self.import_user_timeline
    read_user_timeline().each do |status_struct_obj|
      next if Status.where(:status_id => status_struct_obj.id).exists?
      import_1_status(status_struct_obj, Status::Source::USER_TIMELINE)
    end
  end

  def self.import_friends_timeline
    read_friends_timeline().each do |status_struct_obj|
      next if Status.where(:status_id => status_struct_obj.id).exists?
      import_1_status(status_struct_obj, Status::Source::FRIENDS_TIMELINE)
    end
  end


  def self.import_mentions
    read_mentions().each do |status_struct_obj|
      next if Status.where(:status_id => status_struct_obj.id).exists?
      import_1_status(status_struct_obj, Status::Source::MENTIONS)
    end
  end


private

#--------------------------------------------------------------------------------
  def self.base_options
    {:screen_name => APP_CONFIG['twitter_account']['screen_name']}
  end

  def self.user_timeline_options
    {:status_id => Status.from_friends_timeline.maximum(:status_id)}
  end

  def self.friends_timeline_options
    {:status_id => Status.from_user_timeline.maximum(:status_id)}
  end

  def self.mentions_options
    {:status_id => Status.from_mentions.maximum(:status_id)}
  end

#--------------------------------------------------------------------------------

  def self.read_user_timeline()
    options = base_options.merge(user_timeline_options)
    with_retry do
      client.statuses.user_timeline?(options)
    end
  end

  def self.read_friends_timeline()
    options = base_options.merge(friends_timeline_options)
    with_retry do
      client.statuses.friends_timeline?(options)
    end
  end

  def self.read_mentions()
    options = base_options.merge(mentions_options)
    with_retry do
      client.statuses.mentions?(options)
    end
  end


  def self.import_1_status(t, source)
    screen_name = t.user.screen_name
    name        = t.user.name

    friend = Friend.find_by_screen_name(screen_name) ||
             Friend.create(:screen_name => screen_name, :name => name)

    Status.create!(
      :status_id          => t.id,
      :screen_name        => screen_name,
      :name               => name,
      :content            => t.text,
      :profile_image_url  => t.user.profile_image_url,
      :status_created_at  => DateTime.parse(t.created_at),
      :source             => source,
      :friend_id          => friend.id
    )
  end

  def self.client
    c = Grackle::Client.new(:auth=>{
      :type           =>  :oauth,
      :consumer_key   =>  APP_CONFIG["omniauth"][:consumer_key   ],
      :consumer_secret=>  APP_CONFIG["omniauth"][:consumer_secret],
      :token          =>  APP_CONFIG["omniauth"][:token          ],
      :token_secret   =>  APP_CONFIG["omniauth"][:token_secret   ]
    })
  # c.transport.debug = true
    c

  end


end