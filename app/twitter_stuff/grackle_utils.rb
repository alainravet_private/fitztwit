module GrackleUtils


  # Retry a block of code (default=3 times) if it caused a Grackle::TwitterError.
  # Twitter sometimes fails for no good reason, and you should retry a few times.
  #
  def with_retry(default=nil,attempts=3)
    yield if attempts > 0
  rescue => e
    puts "Received error #{e}"
    if should_retry?(e)
      puts "Will attempt #{attempts-1} more time(s)."
      attempts -= 1
      retry
    else
      raise e
    end
  end

private
  def should_retry?(err)
    if err.kind_of?(Grackle::TwitterError)
      #Satus of nil means it was an unexpected error generally
      #500 errors are probably transient
      err.status.nil? || err.status.to_i >= 500
    else
      false
    end
  end
end
