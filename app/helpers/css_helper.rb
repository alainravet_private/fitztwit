module CssHelper

  def diplay_none_if(condition)
    condition ? 'display:none' : nil

  end

  def diplay_none_unless(inv_condition)
    inv_condition ? nil : 'display:none'
  end
end