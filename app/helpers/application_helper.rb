module ApplicationHelper
  def body_id
    [controller.controller_name, controller.action_name].join('_')
  end

  def body_classes
    [controller.controller_name, controller.action_name].join(' ')
  end

  def title
    case body_id
      when 'console_index'  then 'FitzDares Twitter Console'
      else body_id
    end
  end

  def display_content_with_links(tweet)
    tweet.content.gsub(/(http:\/\/[a-zA-Z0-9\/\.\+\-_:?&=]+)/) {|a| "<a href=\"#{a}\">#{a}</a>"}
  end


  def age_of(o, suffix=' ago')
    time_ago_in_words(o.created_at) + suffix
  end


  def button_to_approve(m)
    m.approvable? ?
      link_to('Approve', {:controller => :messages,  :action => :approve, :id => m.id}, {:method => :put, :remote => true, :class => 'approve'}) :
      '<del title="This message cannot be approved">Approve</del>'
  end

  def button_to_edit(m)
    m.editable? ?
      link_to('Edit', {:controller => :messages,  :action => :start_editing, :id => m.id}, {:method => :put, :remote => true, :class => 'edit'}) :
      '<del title="This message cannot be edited">Edit</del>'
  end


  def button_to_delete(m)
    m.destroyable? ?
      link_to('Delete', m, {:method => :delete, :remote => true, :confirm => confirm_text(:delete, m), :class => 'cancel'}) :
      '<del title="This message cannot be deleted">Delete</del>'
  end


  def topbar_title
    t('console.topbar_title')
  end

  def options_for_users(users, current_user)
    users = [current_user] + (users - [current_user])

    users.
      collect do |u|
      selected = u==current_user
      [u.login, login_path(:login => u.login), selected]
    end.
      collect do |l, p, is_current_user|
      is_current_user ?
        "<option disabled=\"disabled\" selected=\"selected\" value='#{p}'>#{l}</option>" :
        "<option value='#{p}'>#{l}</option>"
    end.
      join
  end

private
  def confirm_text(action, message)
    creator = current_user == message.author ? 'you' : "#{message.author.login}"


    base = case action
      when :delete then "delete a message \ncreated #{age_of(message)} \nby #{creator}"

    end

    "You are about to \n\n#{base}.\n\nAre you sure?"

  end
end
