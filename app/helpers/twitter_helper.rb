module TwitterHelper

  def linkified_twitter_nickname(user_or_nickname)
    nickname = user_or_nickname.is_a?(User) ?
      user_or_nickname.nickname :
      user_or_nickname
    %Q|@<a class="twitter-anywhere-user" href="http://twitter.com/#{nickname}">#{nickname}</a>|.html_safe
  end

end