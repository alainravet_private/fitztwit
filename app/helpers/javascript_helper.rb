module JavascriptHelper

  ALL_JS_FILES =
    [
      'jquery-1.5.0.min',
      'jquery-ui-1.8.9.min',
      'rails',
      'application',
      'json2', 'remote_jquery',
    ].freeze

  def js_files
   ALL_JS_FILES
  end
end