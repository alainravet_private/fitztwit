module RailsLikeHelper

  # Display 2 pieces of info in 1 span tag: the <em>base_info</em> directly, and
  # the <em>hover_info</em> when hovering over the span tag.
  # Usage :
  #   <%= titled_span_tag('Now', Time.now.to_s(:short) %>
  def titled_span_tag(base_info, hover_info, span_options={})
    content_tag(:span, span_options.merge(:title => hover_info)) do
      base_info
    end
  end


  def link_to_or_span_if(condition, name, options = {}, html_options = {}, &block)
    if condition
      '<span class="current">'+ if block_given?
                                  block.arity <= 1 ? yield(name) : yield(name, options, html_options)
                                else
                                  name
                                end + '</span>'
    else
      link_to(name, options, html_options)
    end
  end

  # Usage : same as 'link_to_unless_current'
  # Effect: same as 'link_to_unless_current', except that it wraps the text in
  #   <span class="current">home</span>, when it links to the current page
  # (instead of just inserting the text).

  def link_to_or_span_if_current(name, options = {}, html_options = {}, &block)
    condition = current_page?(options) && :delete != html_options[:method]
    link_to_or_span_if(condition, name, options, html_options, &block)
  end


  # a by-default disabled button_to protected by an Accept-like checkbox
  #
  # Usage :
  # <code>
  #   ...
  #   - btn_id  = "btn_delete_all_users"
  #   - name    = 'Delete all users and accounts'
  #   - options = {:action => :delete_all_asp_users, :asp_id => @asp.to_param}
  #   = button_to_with_accept_tag( name, options, {:id => btn_id})
  # </code>

  def button_to_with_accept_tag(name, options, html_options)
    raise "you must specify an id" unless html_options[:id]
    render  :partial => "_shared/submit_tag_with_accept",
            :locals   => {:name => name, :options => options, :html_options => html_options}
  end

  def link_to_close
    link_to_function icon_tag('close.png'), :onclick => '$(this).parent().hide();', :title => t('actions.close'), :class => 'close'
  end
end
