module ConsoleHelper


  def humanized_state_of(message)
    state = message.state
    case state
      when Message::State::BEING_EDITED
        t('console.label.being_edited_by_', :user_name => message.editor.login)

      when Message::State::PENDING
        (message.author== User.current) ?
          t('console.label.pending_by_you') :
          t('console.label.pending_by_else' , :user_name => message.author.login)

      when Message::State::APPROVED
        (message.approver== User.current) ?
          t('console.label.approved_by_you') :
          t('console.label.approved_by_else', :user_name => message.approver.login)

      when Message::State::TWEETED
          t('console.label.tweeted')
    end
  end
end
