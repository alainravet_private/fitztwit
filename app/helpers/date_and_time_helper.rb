module DateAndTimeHelper


  # ex:  => 19h30
  def time_of(date, format=:just_the_time)
    date.to_s(format)
  end

  def my_time_ago_in_words(time)
    "#{time_ago_in_words(time).
            gsub('about ','').
            gsub('less than ','')
    } ago"
  end

end