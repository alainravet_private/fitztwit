module AjaxHelper


  #
  #  reload a blank/clean #submit_forms html
  #
  def reset_the_submit_forms_div
    render :update do |page|
      page << %Q|$('#submit_forms').html("#{escape_javascript(render :partial => 'messages/submit_forms.html')}")|
    end
  end


  #
  #  reload the list of non-approved messages
  #
  def reload_the_list_of_non_approved_messages
    render :update do |page|
      page << %Q|$('#pendings').html("#{escape_javascript(render Message.non_approved)}")|
    end
  end


  #
  #  reload the list of non-approved messages
  #
  def refresh_message_html(message)
    render :update do |page|
      page << %Q|$('##{dom_id(message)}').html('#{escape_javascript(render message)}');|
    end
  end


# --------------


  def hilite_message(message)
    render(:update) { |page| page << %Q|$('##{dom_id(message)}').effect("highlight", {}, 5000);| }
  end

  def remove_message(message)
    render(:update) { |page| page << %Q|$('##{dom_id(message)}').remove();| }
  end


# --------------


  def enable_the_edit_buttons
    render(:update) { |page| page << "$('.action a.edit').show()" }
  end

  def disable_the_edit_buttons
    render(:update) { |page| page << "$('.action a.edit').hide()" }
  end

end
