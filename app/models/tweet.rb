require 'grackle'
class Tweet < ActiveRecord::Base

  def status_pair
    [screen_name, content]
  end

  # Connect to the Twitter API and pull down the latest tweets
  #
  def self.import_latest_tweets(screen_name)
    StatusFetcher.import_user_timeline
    tweets = client.statuses.user_timeline? :screen_name => screen_name # hit the API
    tweets.each do |t|
      created = DateTime.parse(t.created_at)
      if  Tweet.find_by_tweet_id(t.id)
      else
        Tweet.create(
          :tweet_id           => t.id,
          :screen_name        => t.user.screen_name      ,
          :name               => t.user.name             ,
          :profile_image_url  => t.user.profile_image_url,
          :content            => t.text             ,
          :created            => created
        )
      end
      
    end
  end

  def self.import_friends_timeline(screen_name)
    StatusFetcher.import_friends_timeline
    tweets = client.statuses.friends_timeline? :screen_name => screen_name # hit the API
    tweets.each do |t|
      created = DateTime.parse(t.created_at)
      if  Tweet.find_by_tweet_id(t.id)
      else
        Tweet.create(
          :tweet_id           => t.id,
          :screen_name        => t.user.screen_name      ,
          :name               => t.user.name             ,
          :profile_image_url  => t.user.profile_image_url,
          :content            => t.text             ,
          :created            => created
        )
      end

    end
  end


  def self.import_mentions(screen_name)
    StatusFetcher.import_mentions
    tweets = client.statuses.mentions?
    tweets.each do |t|
      created = DateTime.parse(t.created_at)
      if  Tweet.find_by_tweet_id(t.id)
      else
        Tweet.create(
          :tweet_id           => t.id,
          :screen_name        => t.user.screen_name      ,
          :name               => t.user.name             ,
          :profile_image_url  => t.user.profile_image_url,
          :content            => t.text             ,
          :created            => created
        )
      end

    end
  end

  private
  def self.client
    c = Grackle::Client.new(:auth=>{
      :type           =>  :oauth,
      :consumer_key   =>  APP_CONFIG["omniauth"][:consumer_key   ],
      :consumer_secret=>  APP_CONFIG["omniauth"][:consumer_secret],
      :token          =>  APP_CONFIG["omniauth"][:token          ],
      :token_secret   =>  APP_CONFIG["omniauth"][:token_secret   ]
    })
  # c.transport.debug = true
    c

  end
end