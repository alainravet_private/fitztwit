class Status < ActiveRecord::Base

  module Source
    USER_TIMELINE     = 'ut'
    FRIENDS_TIMELINE  = 'ft'
    MENTIONS          = 'me'
  end

  scope :from_mentions,         where(:source => Source::MENTIONS         )
  scope :from_user_timeline,    where(:source => Source::USER_TIMELINE    )
  scope :from_friends_timeline, where(:source => Source::FRIENDS_TIMELINE )

  belongs_to :friend

  validates :status_id        , :presence => true
  validates :status_created_at, :presence => true
  validates :source           , :presence => true
  validates :screen_name      , :presence => true
  validates :name             , :presence => true
  validates :profile_image_url, :presence => true
  validates :content          , :presence => true


  scope :ours, where(:screen_name => APP_CONFIG.our_screen_name)

  def status_pair
    [screen_name, content]
  end

end
