class Friend < ActiveRecord::Base
  has_many :statuses

  validates :screen_name, :presence => true
  validates :name       , :presence => true
end
