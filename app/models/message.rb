
class Message < ActiveRecord::Base

  class InvalidOperationError < StandardError ; end

  concerned_with :fake_data

  before_validation :set_author_to_current_user, :on => :create

  belongs_to :author  , :class_name => "User", :foreign_key => "created_by"
  belongs_to :editor  , :class_name => "User", :foreign_key => "being_edited_by"
  belongs_to :approver, :class_name => "User", :foreign_key => "approved_by"

  validates :created_by, :presence => true
  validates :body      , :presence => true

  validate  :both_the_retweet_id_and_status_id_are_present_or_absent
  validate  :both_the_in_reply_to_screen_name_and_status_id_are_present_or_absent


  #----------

  def in_reply_to=(status)
    self.in_reply_to_screen_name = status.screen_name
    self.in_reply_to_status_id   = status.status_id
  end

  def in_reply_to
    Status.find_by_status_id(in_reply_to_status_id)
  end

  def reply?
    in_reply_to_screen_name.present?
  end

  #----------

  def retweet_source=(status)
    self.retweet_source_id        = status.id
    self.retweet_source_status_id = status.status_id
  end

  def retweet_source
    Status.find_by_id(retweet_source_id)
  end

  def retweet?
    retweet_source_id.present?
  end

private

  def set_author_to_current_user
    self.author = User.current
  end
end



#*******************************************************************************
#*******************************************************************************
#**                            State Machine                                  **
#*******************************************************************************
#*******************************************************************************

class Message

#-------------------------------------------------------------------------------

  before_validation :set_initial_sm_state, :on => :create

  module State
    PENDING       = 'pending'
    BEING_EDITED  = 'being_edited'
    APPROVED      = 'approved'
    TWEETED       = 'tweeted'
    ALL = [PENDING, APPROVED, BEING_EDITED, TWEETED]
  end

  scope :approved    , where(:state => State::APPROVED)
  scope :non_approved, where(:state => [Message::State::PENDING, Message::State::BEING_EDITED])


  def pending?      ; State::PENDING      == state  end
  def approved?     ; State::APPROVED     == state  end
  def being_edited? ; State::BEING_EDITED == state  end
  def tweeted?      ; State::TWEETED      == state  end

  validates :state, :presence => true, :inclusion => {:in => State::ALL }

#-------------------------------------------------------------------------------

  def self.new_for_retweet(status)
    new(:body => status.content, :retweet_source => status)
  end

  def approvable?
    author_is_not_current_user? && pending?
  end

  def editable?
    pending? && !retweet?
  end

  def updatable?
    editor && editor == User.current
  end

  def tweetable?
    approved?
  end

  def destroyable?
    pending? ||
    approved?   # <- use case : Twitter is down for hours, and urgent approved messages
                #     should no longer be tweeted
  end

#-------------------------------------------------------------------------------

  def approve
    update_attributes!(
            :state      => State::APPROVED,
            :approved_by=> User.current.id
    )
  end

  def start_editing
    raise InvalidOperationError.new("retweets cannot be edited") if retweet?
    update_attributes!(
            :state           => State::BEING_EDITED,
            :being_edited_by => User.current.id,
            :being_edited_at => Time.now
    )
  end


  def end_editing
    raise InvalidOperationError.new("retweets cannot be edited") if retweet?
    if body_changed?
      update_attributes!(
              :state            => State::PENDING,
              :created_by       => User.current.id,
              :being_edited_by  => nil,
              :being_edited_at  => nil
      )
    else
      update_attributes!(
              :state            => State::PENDING,
              :being_edited_by  => nil,
              :being_edited_at  => nil
      )
    end
  end

  def mark_as_tweeted
    update_attributes!(
            :state      => State::TWEETED
    )
  end

#-------------------------------------------------------------------------------
private

  def set_initial_sm_state
   self.state = State::PENDING
  end

  def author_is_not_current_user?
    author != User.current
  end

  def both_the_retweet_id_and_status_id_are_present_or_absent
    rsid, rssid = retweet_source_id, retweet_source_status_id
    if rsid && rssid.nil?
      errors[:retweet_source_status_id] << "is missing"
    elsif rsid.nil? && rssid
      errors[:retweet_source_id       ] << "is missing"
    end
  end

  def both_the_in_reply_to_screen_name_and_status_id_are_present_or_absent
    rsid, rssid = in_reply_to_screen_name, in_reply_to_status_id
    if rsid.present? && rssid.nil?
      errors[:in_reply_to_status_id] << "is missing"
    elsif rsid.blank? && rssid
      errors[:in_reply_to_screen_name] << "is missing"
    end
  end
end
