require 'faker'

class Message

  def self.inject_5_fake_messages
    old_cu = User.current

    u1, u2, u3 = User.limit(3).all

    u1.make_current
    m1 = Message.create! :body => fake_tweet_140_text
    Message.create! :body => fake_tweet_140_text


    u2.make_current
    m2 = Message.create! :body => fake_tweet_140_text
    Message.create! :body => fake_tweet_140_text
    m2.start_editing



    u3.make_current
    m3 = Message.create! :body => fake_tweet_140_text
    Message.create! :body => fake_tweet_140_text
    m3.start_editing

    old_cu.try(:make_current)
  end

private
  def self.fake_tweet_140_text
    length = 40 + rand(101)
    Faker::Lorem.sentence(20)[0..length-1]
  end

end