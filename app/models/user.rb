class User < ActiveRecord::Base

  include SentientUser

  validates :login, :presence => true

  has_many :messages             , :foreign_key => "created_by"
  has_many :being_edited_messages, :foreign_key => "being_edited_by", :class_name => "Message"
end
