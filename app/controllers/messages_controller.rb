#
# Note: this controller will only be used via AJAX -> no html view rendering, no redirect, etc..
#

class MessagesController < ApplicationController

  before_filter :require_login

  def index
    @messages = Message.all
  end


  def create
    @message = Message.new(params[:message])
    if @message.retweet?
      @message.body= @message.retweet_source.content #anti-hack
    end
    @success = @message.save
    if @success
      remote_jquery.call_update_pendings
      remote_jquery.selector(@message).effect('highlight')
    end
  end


  def create_retweet_from
    status_to_retweet = Status.find_by_status_id(params[:status_id])

    @message = Message.new_for_retweet(status_to_retweet)
    @success = @message.save

    remote_jquery.call_update_pendings
    remote_jquery.selector(@message).effect('highlight')
  end



  def update
    render :js => "alert('404 : meetings#update');"
  end


  def approve
    @message = Message.find(params[:id])
    if @message.approvable?
      if @message.author != User.current
        @message.approve

        remote_jquery.selector(@message).remove
        remote_jquery.call_update_approved

        head(:ok) # better than render :nothing => true
      else
        signal_error_to_user t("console.error.author_cannot_approve_his_own_message")
      end
    else
      signal_error_to_user t("console.error.cannot_approve_non_approvable_message")
    end
  end


  def start_editing
    @message = Message.find(params[:id])
    if @message.editable?
      @message.start_editing

      new_html = render_to_string :partial => 'messages/message.html.haml',:object => @message
      remote_jquery.selector(@message).html(new_html)
      remote_jquery.selector(@message).effect('highlight')

    else
      signal_error_to_user t("console.error.cannot_edit_an_uneditable_message")
    end
  end


  def end_editing
    @message = Message.find(params[:id])
    if @message.being_edited?
      if @message.editor == User.current
        if params[:message]
          @message.body = params[:message][:body]
        else
          #no changes <<-- user pressed [cancel]
        end
        @message.end_editing

        new_html = render_to_string :partial => 'messages/message.html.haml',:object => @message
        remote_jquery.selector(@message).html(new_html)
        remote_jquery.selector(@message).effect('highlight')

      else
        signal_error_to_user t("console.error.only_editor_can_complete_edition")
      end

    else
      signal_error_to_user t("console.error.cannot_update_a_not_being_edited_message")
    end

  end


  def destroy
    @message = Message.find(params[:id])

    @message.destroy if @message.destroyable?
    if @message.destroyed?
      remote_jquery.selector(@message).slideToggleAndRemove('fast')

    elsif !@message.destroyable?
      signal_error_to_user(t("console.error.cannot_destroy_an_undestroyable_message"))
    else
      signal_error_to_user(t("console.error.cannot_destroy_a_destroyable_message"))
    end
    head(:ok) # better than render :nothing => true
  end

end
