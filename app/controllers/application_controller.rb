class ApplicationController < ActionController::Base

  include SentientController
  concerned_with  :themes

  protect_from_forgery


  # --------------------------------------------------------------------------------
  #                                      LOGIN
  # --------------------------------------------------------------------------------

  def current_user
    @_current_user ||= session[:user_id] && User.find_by_id(session[:user_id])
  end
  helper_method :current_user

  # PLACEHOLDER : TODO : replace by real login system
  def require_login
    if current_user.nil?
      redirect_to login_path
    end
  end


  # --------------------------------------------------------------------------------
  #                                ERRORS HANDLING
  # --------------------------------------------------------------------------------

  def signal_error_to_user(msg)
    @signaled_error_msg = msg
    render :partial => '_shared/signal_error', :locals => {:msg => @signaled_error_msg}
  end



  # --------------------------------------------------------------------------------
  #                                     WEBSOCKETS
  # --------------------------------------------------------------------------------


  def remote_jquery
    ::RemoteJquery.new(APP_CONFIG['broadcaster']['host'], APP_CONFIG['broadcaster']['port'])
  end

end
