#for concerned_with :themes
class ApplicationController < ActionController::Base


  THEME_1 = "theme_1.css"
  THEME_2 = "theme_2.css"
  DEFAULT_THEME = THEME_1

  # --------------------------------------------------------------------------------
  #                                       themes
  # --------------------------------------------------------------------------------

  def toggle_theme
    new_theme = (current_theme == THEME_1) ? THEME_2 : THEME_1
    cookies["theme"] = new_theme
  
    redirect_to root_path
  end

  def current_theme
    cookies["theme"] || DEFAULT_THEME
  end
  helper_method :current_theme
end
