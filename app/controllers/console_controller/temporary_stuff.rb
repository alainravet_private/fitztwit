# for concerned_with  :temporary_stuff

class ConsoleController < ApplicationController

  def inject_fake_messages
    Message.inject_5_fake_messages
    redirect_to :action => :index
  end

  def pull_timeline
    StatusFetcher.import_all
    flash[:notice] = 'a notice about pull_timeline'
    redirect_to :action => :index
  end

  def tweet_all_approved
    result = StatusUpdater.tweet_all_approved_messages
    if :ok == result
      flash[:notice] = "Success (:ok)"
    else
      error, http_code, msg = result
      flash[:error] = result.inspect
      Rails.logger.error result.inspect
    end

    redirect_to :action => :index
  end

  def import_timeline_changes
    nof_f, nof_s = Friend.count, Status.count

    StatusFetcher.import_all
    render :update do |page|
      new_f, new_s = Friend.count-nof_f, Status.count-nof_s
      msg = []
      msg << pluralize(new_f, 'new friend') if 1 <= new_f
      msg << pluralize(new_s, 'new status') if 1 <= new_s
      if msg.empty?
        page.alert("done (no new data)")
      else
        page.alert("Timeline updated : #{msg.join(',')}")
      end
    end
  end
end
