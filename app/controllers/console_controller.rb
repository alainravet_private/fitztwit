class ConsoleController < ApplicationController
  concerned_with  :temporary_stuff

  before_filter :require_login, :except => :login

  def index
    @timeline_2= Status.order('status_created_at desc').limit(40).all

    @non_approved_messages= Message.non_approved
    @approved_messages    = Message.approved
    @being_edited_message = current_user.being_edited_messages.try(:first)
  end

  def login
  end


  def update_timeline
    @last_fetched_at = params[:last_fetched_at] && Time.at(params[:last_fetched_at].to_i)
    @no_new_data = @last_fetched_at && Status.last.created_at <= @last_fetched_at

    @timeline_2= Status.order('status_created_at desc').limit(16).all
  end


  def update_approved
    @last_fetched_at = params[:last_fetched_at] && Time.at(params[:last_fetched_at].to_i)
    @no_new_data = false #@last_fetched_at && Status.last.created_at <= @last_fetched_at

    @approved_messages    = Message.approved
  end

  def update_pendings
    @non_approved_messages= Message.non_approved
  end

end
