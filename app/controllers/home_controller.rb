class HomeController < ApplicationController
  def index
  end

  def login
    user = User.find_by_login(params[:login]) || fake_default_user
    session[:user_id  ] = user && user.id

    redirect_to request.referer || root_path
  end

  def fake_default_user
    User.first
  end
end


