require 'yaml'
require 'lib/core_extensions/hash'
require 'capistrano/ext/multistage'
require 'activesupport'

set :stages, %w(staging)
unset :deploy_to

set :scm, :git
set :repository, 'gitosis@kapoq.com:fitz-twit'
set :git_enable_submodules, true
set :deploy_via, :remote_cache

set :chef_config, '/etc/chef/client.rb'

default_run_options[:pty] = true

namespace :cron do
  task :update do
    # overwrite me
  end
end

namespace :deploy do
  before "deploy:update", "chef:run_client"
  after "deploy:setup", "utils:create_other_shared_folders"
  after "deploy:setup", "utils:chown_it_all"

  after "deploy:symlink", "utils:symlink_other_shared_folders"

  before "deploy:restart", "utils:write_deployment"
  before "deploy:restart", "utils:ensure_log_files_exist"
  before "deploy:restart", "utils:make_deploy_path_group_writable"
  before "deploy:restart", "utils:make_web_group_own_current_path"
  before "deploy:restart", "utils:chown_environment"
  before "deploy:restart", "deploy:migrate"
  before "deploy:migrate", "db:configure"
  after  "db:configure",   "utils:install_gems"

  task :restart do
    # noop
  end

  after "deploy:restart", "deploy:restart_passenger"
  task :restart_passenger, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end
end
