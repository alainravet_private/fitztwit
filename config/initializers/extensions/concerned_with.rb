#---------------------
# concerns :
#---------------------
# source:
#   http://github.com/courtenay/altered_beast/tree/master/config/initializers/concerns.rb
#   http://m.onkey.org/2008/9/15/active-record-tips-and-tricks
#
# Usage in classes :
#   concerned_with :caching, :loging
#
# Usage in modules :
#   module Media
#   	def self.included(base)
#       base.concerned_with  :opinions_and_favorites, :owner => Media  # or self
#     end
#   ..
#   end

class << Object
  def concerned_with(*concerns)
	  options = concerns.extract_options!
		options.assert_valid_keys(:owner)

	  subdir_name = options[:owner] ?
					  Inflector.underscore(options[:owner]) :
					  name.underscore

    concerns.each do |concern|
      require_dependency "#{subdir_name}/#{concern}"
    end
  end
end
