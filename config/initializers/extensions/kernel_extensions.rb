module Kernel
  # usage:
  #   year = to_i_or_nil(params[:year])
  #
  def to_i_or_nil(value)
    value && value.to_i rescue nil
  end


  def print_stacktrace
    raise
  rescue
      puts $!.backtrace[1..-1].join("\n")
  end

end
