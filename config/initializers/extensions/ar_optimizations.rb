class ActiveRecord::Base

  # Usage
  #   ids = User.collect_ids
  #   ids = asp.users.collect_ids
  def self.collect_ids
    all(:select => "#{self.table_name}.id").collect{|u| u.id}
  end
end