class ActiveRecord::Base

  def dom_id(prefix = nil)
    "#{prefix}#{self.class.name.underscore}_#{self.id}"
  end

  def age_in_seconds
    (Time.now - created_at).round
  end

  # Usage
  #   ids = User.collect_ids
  #   ids = asp.users.collect_ids
  def self.collect_ids
    all(:select => "#{self.table_name}.id").collect{|u| u.id}
  end

end
