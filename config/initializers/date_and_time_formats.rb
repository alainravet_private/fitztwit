# see : http://snippets.dzone.com/posts/show/5523
Time::DATE_FORMATS.merge!(

        # 19h30
        :just_the_time => "%Hh%M",

        # 19h30:01
        :just_the_time_with_seconds => "%Hh%M:%S",

        # 30 Nov 12:34:56
        :short_with_seconds => "%d %b %H:%M:%S"
)
