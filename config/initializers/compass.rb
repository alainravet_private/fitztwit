require 'compass'
require 'compass/app_integration/rails'
Compass::AppIntegration::Rails.initialize!

#Sass::Plugin.options[:style] = :compressed
Sass::Plugin.options[:style] = :expanded

Sass::Plugin.options[:line_comments] = false
Sass::Plugin.options[:debug_info] = true