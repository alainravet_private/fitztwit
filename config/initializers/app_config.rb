
def get_local_ip_address
  # same code as in lib/websockets/utils.rb
  require 'socket'
  UDPSocket.open {|s| s.connect('64.233.187.99', 1); s.addr.last }
end


# Set up APP_CONFIG[] hash from app_config.yml

# Usage:  
#   twitter_key = APP_CONFIG["omnyauth"][:consumer_key]
APP_CONFIG = (YAML.load_file("#{Rails.root}/config/app_config.yml") || {}).with_indifferent_access[Rails.env]
APP_CONFIG['broadcaster']['host']||=get_local_ip_address


def APP_CONFIG.our_screen_name
  @@_our_screen_name = APP_CONFIG['twitter_account']['screen_name']
end
