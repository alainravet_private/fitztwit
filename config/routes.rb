Fitztwit::Application.routes.draw do

  root :to => "console#index"

  #----------------------------------------------------------------------

  get "home/index"
  get "login", :controller => 'home', :action => 'login', :as => 'login'


  get  "console/index"

  #for browser-triggered auto-refresh :
  get  "console/update_approved"
  get  "console/update_pendings"
  get  "console/update_timeline"

  get  '/toggle_theme' , :controller => 'application', :action => 'toggle_theme'

  resources :messages, :only => [:index, :create, :update, :destroy] do
    member do
      put  'start_editing'
      put  'end_editing'
      put  'approve'
    end
    collection do
      post 'create_retweet_from/:status_id', :action => 'create_retweet_from', :as => :create_retweet_from
    end
  end

  #----------------------------------------------------------------------

  # dev-only :
  get  "console/sprite"


  #----------------------------------------------------------------------
  # temporary_stuff
  post "console/inject_fake_messages"
  post "console/pull_timeline"
  post "console/tweet_all_approved"
  post "console/import_timeline_changes"


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
