# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Fitztwit::Application.initialize!



# ----------------------------------------------------------------------
# --  preconditions checks  : should we let the app. start            --
# ----------------------------------------------------------------------
FailFast().check do
# ----------------------------------------------------------------------

  in_test_mode = !%w(production development).include?(Rails.env)

  in_rake_command =
    begin
      raise
    rescue
      txt = $!.backtrace.last   # ex: /usr/local/bin/rake:19
      !!(txt =~ /\/rake:/)
    end

  skip_if in_test_mode || in_rake_command do
    begin
      host = APP_CONFIG['broadcaster']['host']
      port = APP_CONFIG['broadcaster']['port']
      WebSocket.new("ws://#{host}:#{port}")

    rescue (Errno::ECONNREFUSED)
      fail "Websocket Broadcaster not found : host= #{host}, port=#{port}"
    end
  end

end
