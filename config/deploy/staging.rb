unless exists?(:deploy_to)
  raise "Please supply a deploy_to argument e.g. cap spreads_staging ci:before_test_run -s deploy_to=$WORKSPACE"
end

server "staging.fitzdares.com", :web, :app, :db, :primary => true

set :application, "twitter"
set :rails_env, "staging"
set :branch, 'master'
set :current_dir, ''

set :fqdn, 'fitzdatastage.fitzdares.local'

set :user, "data"